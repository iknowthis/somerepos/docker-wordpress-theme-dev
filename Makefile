dcdev=docker-compose -f docker-compose.yml -f docker-compose.dev.yml

all: clean build

build:
	${dcdev} build $(c)

rundev:
	${dcdev} up $(c)

run:
	docker-compose up -d

clean:
	-${dcdev} rm -sfv

clean-hard: clean
	-${dcdev} down --rmi all -v