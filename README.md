# Wordpress Theme Development on Docker

## Quick start:

> 1. build localhost/wordpress:5-php7.1-apache image

```sh

make

```

> 2. Up Wordpress, MySQL, Adminer containers

```sh

make rundev

```

> 3. Open on browser Wordpress: http://localhost:8080 Adminer: http://localhost:9090

## Usage

### Dev

Just use [Quick start](#quick-start) guide

### Prod

...
